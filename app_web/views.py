from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from app_auth import forms as auth_form
from app_status import views as status_views
from app_friend import views as friend_views
from app_profile import views as profile_views
from core import strings

# Create your views here.

def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('web:dashboard'))

    html = 'auth/login.html'
    response = {
        'TITLE': strings.TITLE,
        'page_title': 'Welcome',
        'YEAR': strings.YEAR,
        'login_form': auth_form.LoginForm,
    }

    return render(request, html, response)


def index_dashboard(request):
    if 'user_login' not in request.session:
        return HttpResponseRedirect(reverse('web:index'))

    html = 'web/index.html'
    response = {
        'TITLE': strings.TITLE,
        'page_title': 'Dashboard',
        'YEAR': strings.YEAR,
        'user_login': request.session['user_login']
    }

    status_views.index(request, response)
    friend_views.index(request, response)
    profile_views.index(request, response)

    print(request.session['user_login'])

    return render(request, html, response)
